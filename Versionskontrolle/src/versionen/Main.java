package versionen;
/*
 * 	VERSIONSKONTROLLE
 * 
 * 
 * 		-> Konfliktfreie Bearbeitung und Verwaltung von Dateien durch mehrere Instanzen
 * 		   �ber die gesamte Entwicklungszeit, wobei jede Variante der Dateien als Historie
 * 		   gespeichert wird
 * 
 * 
 * 		-> Diese Historie wird in einem sogenannten Repository gehalten(kurz: repo)
 * 
 * 		-> Das Verschmelzen von Varianten einer Bestimmten Datei folgt in jedem
 * 		   Versionskontrollsystem einem bestimmten Vertrag: Dem sog. Mergevertrag
 * 
 * 				-> Das Verscmelzen zu einer gemeinsamen, aktuellen Version nennt man Merge
 * 
 * 
 * 		-> Bei der gesamten Entwicklung gibt es immer einen Haupt-Entwicklungszweig/Hauptast
 * 				-> master
 * 
 * 		-> Nebenentwicklungsstr�nge nennt man "branch"/"branches"
 * 
 * 		-> Jeder Entwicklungsstrang hat immer eine Datei, welche f�r die aktuellste Variante
 * 		   steht, und diese wird "head" genannt
 * 
 * 		-> Beim Merging wird, wenn es Konflikte gibt in den Dateist�nden, der Merge-Vorgang 
 * 		   blockiert, bis der Konflikt von den Entwicklern gel�st wurde
 * 
 * 		-> Bekannte Versionskontrollsysteme:
 * 
 * 				-> TeamFoundation Server(Microsoft)
 * 
 * 				-> SVN
 * 
 * 				-> Git
 * 
 * 		-> Grundlegende Konzepte von Git:
 * 
 * 				-> Jede Datei in einem Repository existiert nur einmal
 * 
 * 				-> Git arbeitet haupts�chlich mit Hash-Werten
 * 
 * 				-> Dateien werden als Schl�ssel angelegt(seg. "Blobs")
 * 
 * 					-> Blobs sind auf Hashing basierende Dateien, welche Unterschiede
 * 					   zur Original-Datei speichern
 * 
 * 				-> Sowohl Dateien als auch die dazugeh�rigen Blobs werden baumartig
 * 				   intern verwaltet
 * 
 * 
 * 		-> Zun�chst wird "nur" im sogenannten "Working directory" gearbeitet!
 * 
 * 				-> �nderungen, die hier stattfinden, werden NICHT sofort/automatisch 
 * 				   ins lokale Repository abgelegt
 * 
 * 				-> Der Befehl COMMIT:
 * 
 * 						- Durchgef�hrte �nderungen werden ins Repository �bernommen
 * 
 * 						- Jeder Commit ist im Prinzip ein "Zeiger" auf den jeweiligen branch, auf 
 * 					      welchen sich dieser Commit bezieht
 * 
 * 
 * 		->	Wenn man mit Git entwickelt, wird normalerweise immer vom Hauptzweig(master) 
 * 			abgezweigt in einem neuen branch
 * 			->	In diesem neuen branch wird solange entwickelt, bis eine Version 
 * 				vorliegt, welche nach Ansicht des Entwicklers "verschmolzen" werden
 * 				kann mit dem lokalen Hauptzweig(master)
 * 
 * 		->	Der Befehl, um zwischen branches zu wechseln, hei�t CHECKOUT 	
 * 
 * 		->	Normalerweise wird regelm��ig (nachdem abgezweigte branches lokal wieder 
 * 			zusammengef�hrt werden) per PUSH die aktuelle Version in Remote-Repo geladen.
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
